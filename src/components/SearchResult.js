import React, { Component } from 'react'
import { SEARCH_RESULT_PLACEHOLDER } from '../constants'

export default class SearchResult extends Component {
    render() {
        return (
            <div>
                <div id="result">
                    {SEARCH_RESULT_PLACEHOLDER}
                </div>
            </div>
        )
    }
}
