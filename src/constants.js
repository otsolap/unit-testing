export const APPLICATION_TITLE = 'Pokemon Application'
export const SEARCH_RESULT_PLACEHOLDER = 'Search result will come here'
export const SEARCH_BTN_TXT = 'Search Now'