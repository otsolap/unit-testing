import './App.css';
import SearchResult from './components/SearchResult';
import {
  APPLICATION_TITLE,
  SEARCH_BTN_TXT
} from './constants'

function App() {
  return (
    <div className="App">
      <h1> {APPLICATION_TITLE} </h1>
      <div id="searchBox">
        <div id="searchBoxBtn">{SEARCH_BTN_TXT}</div>
      </div>
      <SearchResult />
    </div>
  );
}

export default App;
