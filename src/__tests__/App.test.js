import App from '../App';
import { shallow, mount } from 'enzyme'
import {
  APPLICATION_TITLE,
  SEARCH_BTN_TXT
} from '../constants'

describe('Landing page', () => {
  let wrapper;
  // beforeEach testi, tällä otetaan spagettia veks.
  beforeEach(() => {
    wrapper = shallow(<App />)
  })
  it('test', () => {
    expect(wrapper.find('h1').text()).toContain(APPLICATION_TITLE)
  })
  it('loads a #searchBox element', () => {
    expect(wrapper.find('#searchBox').length).toBe(1)
  })
  it('loads a #searchBoxBtn element', () => {
    expect(wrapper.find('#searchBoxBtn').length).toBe(1)
  })
  it('#searchBoxBtn text should read Search Now', () => {
    const searchBoxBtn = wrapper.find('#searchBoxBtn')
    const searchBoxBtnText = searchBoxBtn.text()
    expect(searchBoxBtnText).toContain(SEARCH_BTN_TXT)
  })

  // The difference between shallow() and mount() is that shallow() 
  // tests components in isolation from the child components they render 
  // while mount()goes deeper and tests a component's children. 
  describe('Landing page interaction with user', () => {
    it('searchResult component should have a #result container', () => {
      const wrapper = mount(<App />)
      const resultContainer = wrapper.find('#result')
      console.log(wrapper.debug())
      expect(resultContainer).toHaveLength(1)
    })
  })

})

